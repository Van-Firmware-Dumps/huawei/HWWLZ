# Copyright (C) 2012 The Android Open Source Project
#
# USB configuration common for platform devices
#

on init
    mount configfs none /config
    mkdir /config/usb_gadget/g1
    write /config/usb_gadget/g1/bcdDevice 0x0299
    mkdir /config/usb_gadget/g1/strings/0x409
    chmod 0640 /config/usb_gadget/g1/strings/0x409/serialnumber
    write /config/usb_gadget/g1/strings/0x409/serialnumber ${ro.serialno}
    mkdir /config/usb_gadget/g1/configs/b.1
    write /config/usb_gadget/g1/configs/b.1/MaxPower 0x1f4
    write /config/usb_gadget/g1/configs/b.1/bmAttributes 0xc0
    mkdir /config/usb_gadget/g1/configs/b.1/strings/0x409
    mkdir /config/usb_gadget/g1/functions/ffs.adb
    mkdir /config/usb_gadget/g1/functions/ffs.hdb
    mkdir /config/usb_gadget/g1/functions/mtp.gs0
    write /config/usb_gadget/g1/functions/mtp.gs0/os_desc/interface.MTP/compatible_id "MTP"
    mkdir /config/usb_gadget/g1/functions/ptp.gs1
    mkdir /config/usb_gadget/g1/functions/accessory.gs2
    mkdir /config/usb_gadget/g1/functions/audio_source.gs3
    mkdir /config/usb_gadget/g1/functions/midi.gs5
    mkdir /config/usb_gadget/g1/functions/mass_storage.gs6
    mkdir /config/usb_gadget/g1/functions/ncm.gs7
    write /config/usb_gadget/g1/functions/ncm.gs7/host_addr ${ro.serialno}
    write /config/usb_gadget/g1/functions/ncm.gs7/qmult 64
    mkdir /config/usb_gadget/g1/functions/modem_acm.a_at
    mkdir /config/usb_gadget/g1/functions/modem_acm.acm_3g_diag
    mkdir /config/usb_gadget/g1/functions/modem_acm.acm_a_shell
    mkdir /config/usb_gadget/g1/functions/modem_acm.acm_c_shell
    mkdir /config/usb_gadget/g1/functions/modem_acm.acm_ctrl
    mkdir /config/usb_gadget/g1/functions/modem_acm.acm_4g_diag
    mkdir /config/usb_gadget/g1/functions/modem_acm.acm_gps
    mkdir /config/usb_gadget/g1/functions/modem_acm.acm_cdma_log
    mkdir /config/usb_gadget/g1/functions/modem_acm.acm_skytone
    mkdir /config/usb_gadget/g1/functions/modem_acm.acm_modem
    mkdir /config/usb_gadget/g1/functions/hw_acm.hw_acm_modem
    mkdir /config/usb_gadget/g1/functions/hw_acm.hw_acm_PCUI
    mkdir /config/usb_gadget/g1/functions/hw_acm.hw_acm_DIAG
    mkdir /config/usb_gadget/g1/functions/SourceSink.gs10
    write /config/usb_gadget/g1/os_desc/b_vendor_code 0x1
    write /config/usb_gadget/g1/os_desc/qw_sign "MSFT100"
    symlink /config/usb_gadget/g1/configs/b.1 /config/usb_gadget/g1/os_desc/b.1

    chown system system /sys/class/android_usb/android0/f_rndis/ethaddr
    chmod 0660 /sys/class/android_usb/android0/f_rndis/ethaddr
    write /sys/class/android_usb/android0/f_rndis/wceis  1

# f_fs
    mkdir /dev/usb-ffs 0770 shell shell
    mkdir /dev/usb-ffs/adb 0770 shell shell
    mount functionfs adb /dev/usb-ffs/adb uid=2000,gid=2000
# f_fs_hdb
    mkdir /dev/usb-ffs/hdb 0770 shell shell
    mount functionfs hdb /dev/usb-ffs/hdb uid=2000,gid=2000

# set mtp device_type to Mobile Handset
    setprop sys.usb.mtp.device_type 3

on boot
    setprop sys.usb.configfs 1

on post-fs-data && property:sys.usb.configfs=1
    write /config/usb_gadget/g1/strings/0x409/manufacturer ${ro.product.manufacturer}
    write /config/usb_gadget/g1/strings/0x409/product ${ro.product.model}

on charger-config-usb_gadget && property:sys.usb.configfs=1
    write /config/usb_gadget/g1/strings/0x409/manufacturer ${ro.product.manufacturer}
    write /config/usb_gadget/g1/strings/0x409/product ${ro.product.model}

on property:sys.usb.config=none && property:sys.usb.configfs=1
    write /config/usb_gadget/g1/os_desc/use 0
    stop hdbd
    setprop sys.usb.ffs_hdb.ready 0
    write /sys/devices/virtual/android_usb/android0/port_mode 1

on property:sys.usb.config=adb && property:sys.usb.configfs=1
    write /config/usb_gadget/g1/idVendor 0x12d1
    write /config/usb_gadget/g1/idProduct 0x103A

on property:sys.usb.ffs.ready=1 && property:sys.usb.config=adb && property:sys.usb.configfs=1
    write /sys/devices/virtual/android_usb/android0/port_mode 2

on property:sys.usb.config=mtp && property:sys.usb.configfs=1
    write /config/usb_gadget/g1/idVendor 0x12d1
    write /config/usb_gadget/g1/idProduct 0x107e
    write /sys/devices/virtual/android_usb/android0/port_mode 3

on property:sys.usb.config=mtp,adb && property:sys.usb.configfs=1
    write /config/usb_gadget/g1/idVendor 0x12d1
    write /config/usb_gadget/g1/idProduct 0x107e

on property:sys.usb.ffs.ready=1 && property:sys.usb.config=mtp,adb && property:sys.usb.configfs=1
    write /sys/devices/virtual/android_usb/android0/port_mode 4

on property:sys.usb.config=ptp && property:sys.usb.configfs=1
    write /config/usb_gadget/g1/idVendor 0x12d1
    write /config/usb_gadget/g1/idProduct 0x107e
    write /sys/devices/virtual/android_usb/android0/port_mode 5

on property:sys.usb.config=ptp,adb && property:sys.usb.configfs=1
    write /config/usb_gadget/g1/idVendor 0x12d1
    write /config/usb_gadget/g1/idProduct 0x107e

on property:sys.usb.ffs.ready=1 && property:sys.usb.config=ptp,adb && property:sys.usb.configfs=1
    write /sys/devices/virtual/android_usb/android0/port_mode 6

on property:sys.usb.config=accessory && property:sys.usb.configfs=1
    write /config/usb_gadget/g1/idVendor 0x18d1
    write /config/usb_gadget/g1/idProduct 0x2d00
    write /sys/devices/virtual/android_usb/android0/port_mode 11

on property:sys.usb.config=accessory,adb && property:sys.usb.configfs=1
    write /config/usb_gadget/g1/idVendor 0x18d1
    write /config/usb_gadget/g1/idProduct 0x2d01

on property:sys.usb.ffs.ready=1 && property:sys.usb.config=accessory,adb && property:sys.usb.configfs=1
    write /sys/devices/virtual/android_usb/android0/port_mode 12

on property:sys.usb.config=audio_source && property:sys.usb.configfs=1
    write /config/usb_gadget/g1/idVendor 0x18d1
    write /config/usb_gadget/g1/idProduct 0x2d02
    write /sys/devices/virtual/android_usb/android0/port_mode 21

on property:sys.usb.config=audio_source,adb && property:sys.usb.configfs=1
    write /config/usb_gadget/g1/idVendor 0x18d1
    write /config/usb_gadget/g1/idProduct 0x2d03

on property:sys.usb.ffs.ready=1 && property:sys.usb.config=audio_source,adb && property:sys.usb.configfs=1
    write /sys/devices/virtual/android_usb/android0/port_mode 22

on property:sys.usb.config=accessory,audio_source && property:sys.usb.configfs=1
    write /config/usb_gadget/g1/idVendor 0x18d1
    write /config/usb_gadget/g1/idProduct 0x2d04
    write /sys/devices/virtual/android_usb/android0/port_mode 23

on property:sys.usb.config=accessory,audio_source,adb && property:sys.usb.configfs=1
    write /config/usb_gadget/g1/idVendor 0x18d1
    write /config/usb_gadget/g1/idProduct 0x2d05

on property:sys.usb.ffs.ready=1 && property:sys.usb.config=accessory,audio_source,adb && property:sys.usb.configfs=1
    write /sys/devices/virtual/android_usb/android0/port_mode 24

on property:sys.usb.config=midi && property:sys.usb.configfs=1
    write /config/usb_gadget/g1/idVendor 0x18d1
    write /config/usb_gadget/g1/idProduct 0x4ee9

on property:sys.usb.config=midi,adb && property:sys.usb.configfs=1
    write /config/usb_gadget/g1/idVendor 0x18d1
    write /config/usb_gadget/g1/idProduct 0x4ee9

on property:sys.usb.config=rndis && property:sys.usb.configfs=1
    write /config/usb_gadget/g1/idVendor 0x12d1
    write /config/usb_gadget/g1/idProduct 0x108a
    write /sys/class/android_usb/android0/f_rndis/wceis  1
    write /sys/devices/virtual/android_usb/android0/port_mode 9

on property:sys.usb.config=rndis,adb && property:sys.usb.configfs=1
    write /config/usb_gadget/g1/idVendor 0x12d1
    write /config/usb_gadget/g1/idProduct 0x108a
    write /sys/class/android_usb/android0/f_rndis/wceis  1

on property:sys.usb.ffs.ready=1 && property:sys.usb.config=rndis,adb && property:sys.usb.configfs=1
    write /sys/devices/virtual/android_usb/android0/port_mode 10

on property:sys.usb.config=hisi_debug,adb && property:sys.usb.configfs=1
    write /config/usb_gadget/g1/UDC "none"
    rmdir /config/usb_gadget/g1/functions/rndis.gs4
    write /config/usb_gadget/g1/os_desc/use 1
    write /config/usb_gadget/g1/configs/b.1/strings/0x409/configuration "hisi_debug_adb"
    write /config/usb_gadget/g1/idVendor 0x12d1
    write /config/usb_gadget/g1/idProduct 0x107e
    start adbd

on property:sys.usb.ffs.ready=1 && property:sys.usb.config=hisi_debug,adb && property:sys.usb.configfs=1
    write /sys/class/android_usb/android0/f_rndis/wceis  1
    mkdir /config/usb_gadget/g1/functions/rndis.gs4
    write /config/usb_gadget/g1/functions/rndis.gs4/os_desc/interface.rndis/compatible_id RNDIS
    write /config/usb_gadget/g1/functions/rndis.gs4/os_desc/interface.rndis/sub_compatible_id 5162001
    symlink /config/usb_gadget/g1/functions/rndis.gs4 /config/usb_gadget/g1/configs/b.1/f1
    symlink /config/usb_gadget/g1/functions/modem_acm.a_at /config/usb_gadget/g1/configs/b.1/f2
    symlink /config/usb_gadget/g1/functions/modem_acm.acm_3g_diag /config/usb_gadget/g1/configs/b.1/f3
    symlink /config/usb_gadget/g1/functions/modem_acm.acm_a_shell /config/usb_gadget/g1/configs/b.1/f4
    symlink /config/usb_gadget/g1/functions/modem_acm.acm_c_shell /config/usb_gadget/g1/configs/b.1/f5
    symlink /config/usb_gadget/g1/functions/modem_acm.acm_ctrl /config/usb_gadget/g1/configs/b.1/f6
    symlink /config/usb_gadget/g1/functions/modem_acm.acm_4g_diag /config/usb_gadget/g1/configs/b.1/f7
    symlink /config/usb_gadget/g1/functions/modem_acm.acm_gps /config/usb_gadget/g1/configs/b.1/f8
    symlink /config/usb_gadget/g1/functions/modem_acm.acm_cdma_log /config/usb_gadget/g1/configs/b.1/f9
    symlink /config/usb_gadget/g1/functions/modem_acm.acm_skytone /config/usb_gadget/g1/configs/b.1/f10
    symlink /config/usb_gadget/g1/functions/modem_acm.acm_modem /config/usb_gadget/g1/configs/b.1/f11
    symlink /config/usb_gadget/g1/functions/ffs.adb /config/usb_gadget/g1/configs/b.1/f12
    write /config/usb_gadget/g1/UDC ${sys.usb.controller}
    stop atcmdserver
    start hw_cdma_service
    setprop sys.usb.state ${sys.usb.config}
    write /sys/class/net/rndis0/queues/rx-0/rps_cpus f0

on property:sys.usb.config=hisi_debug && property:sys.usb.configfs=1
    write /config/usb_gadget/g1/UDC "none"
    rmdir /config/usb_gadget/g1/functions/rndis.gs4
    stop adbd
    write /config/usb_gadget/g1/os_desc/use 1
    write /config/usb_gadget/g1/configs/b.1/strings/0x409/configuration "hisi_debug"
    write /config/usb_gadget/g1/idVendor 0x12d1
    write /config/usb_gadget/g1/idProduct 0x107e
    write /sys/class/android_usb/android0/f_rndis/wceis  1
    mkdir /config/usb_gadget/g1/functions/rndis.gs4
    write /config/usb_gadget/g1/functions/rndis.gs4/os_desc/interface.rndis/compatible_id RNDIS
    write /config/usb_gadget/g1/functions/rndis.gs4/os_desc/interface.rndis/sub_compatible_id 5162001
    symlink /config/usb_gadget/g1/functions/rndis.gs4 /config/usb_gadget/g1/configs/b.1/f1
    symlink /config/usb_gadget/g1/functions/modem_acm.a_at /config/usb_gadget/g1/configs/b.1/f2
    symlink /config/usb_gadget/g1/functions/modem_acm.acm_3g_diag /config/usb_gadget/g1/configs/b.1/f3
    symlink /config/usb_gadget/g1/functions/modem_acm.acm_a_shell /config/usb_gadget/g1/configs/b.1/f4
    symlink /config/usb_gadget/g1/functions/modem_acm.acm_c_shell /config/usb_gadget/g1/configs/b.1/f5
    symlink /config/usb_gadget/g1/functions/modem_acm.acm_ctrl /config/usb_gadget/g1/configs/b.1/f6
    symlink /config/usb_gadget/g1/functions/modem_acm.acm_4g_diag /config/usb_gadget/g1/configs/b.1/f7
    symlink /config/usb_gadget/g1/functions/modem_acm.acm_gps /config/usb_gadget/g1/configs/b.1/f8
    symlink /config/usb_gadget/g1/functions/modem_acm.acm_cdma_log /config/usb_gadget/g1/configs/b.1/f9
    symlink /config/usb_gadget/g1/functions/modem_acm.acm_skytone /config/usb_gadget/g1/configs/b.1/f10
    symlink /config/usb_gadget/g1/functions/modem_acm.acm_modem /config/usb_gadget/g1/configs/b.1/f11
    write /config/usb_gadget/g1/UDC ${sys.usb.controller}
    stop atcmdserver
    start hw_cdma_service
    setprop sys.usb.state ${sys.usb.config}
    write /sys/class/net/rndis0/queues/rx-0/rps_cpus f0

# adbd on at boot in emulator
on property:ro.kernel.qemu=1
    start adbd

on property:sys.usb.config=mtp && property:sys.usb.configfs=0
    write /sys/class/android_usb/android0/enable 0
    write /sys/class/android_usb/android0/idVendor 12d1
    write /sys/class/android_usb/android0/idProduct 107e
    write /sys/class/android_usb/android0/functions ${sys.usb.config}
    write /sys/devices/virtual/android_usb/android0/port_mode 3
    write /sys/class/android_usb/android0/enable 1
    setprop sys.usb.state ${sys.usb.config}

on property:sys.usb.config=mtp,hdb && property:sys.usb.configfs=0
    write /sys/class/android_usb/android0/enable 0
    write /sys/class/android_usb/android0/idVendor 12d1
    write /sys/class/android_usb/android0/idProduct 107e
    write /sys/class/android_usb/android0/functions ${sys.usb.config}
    write /sys/devices/virtual/android_usb/android0/port_mode 25
    write /sys/class/android_usb/android0/enable 1
    start hdbd
    setprop sys.usb.state ${sys.usb.config}

on property:sys.usb.config=mtp,adb && property:sys.usb.configfs=0
    write /sys/class/android_usb/android0/enable 0
    write /sys/class/android_usb/android0/idVendor 12d1
    write /sys/class/android_usb/android0/idProduct 107e
    write /sys/class/android_usb/android0/functions ${sys.usb.config}
    write /sys/devices/virtual/android_usb/android0/port_mode 4
    write /sys/class/android_usb/android0/enable 1
    start adbd
    setprop sys.usb.state ${sys.usb.config}

on property:sys.usb.config=mtp,adb,hdb && property:sys.usb.configfs=0
    write /sys/class/android_usb/android0/enable 0
    write /sys/class/android_usb/android0/idVendor 12d1
    write /sys/class/android_usb/android0/idProduct 107e
    write /sys/class/android_usb/android0/functions ${sys.usb.config}
    write /sys/devices/virtual/android_usb/android0/port_mode 26
    write /sys/class/android_usb/android0/enable 1
    start adbd
    start hdbd
    setprop sys.usb.state ${sys.usb.config}

on property:sys.usb.config=rndis && property:sys.usb.configfs=0
    write /sys/class/android_usb/android0/enable 0
    write /sys/class/android_usb/android0/idVendor 12d1
    write /sys/class/android_usb/android0/idProduct 108a
    write /sys/class/android_usb/android0/f_rndis/wceis  1
    write /sys/class/android_usb/android0/functions ${sys.usb.config}
    write /sys/devices/virtual/android_usb/android0/port_mode 9
    write /sys/class/android_usb/android0/enable 1
    write /sys/class/net/rndis0/queues/rx-0/rps_cpus f
    setprop sys.usb.state ${sys.usb.config}

on property:sys.usb.config=rndis,hdb && property:sys.usb.configfs=0
    write /sys/class/android_usb/android0/enable 0
    write /sys/class/android_usb/android0/idVendor 12d1
    write /sys/class/android_usb/android0/idProduct 108a
    write /sys/class/android_usb/android0/f_rndis/wceis  1
    write /sys/class/android_usb/android0/functions ${sys.usb.config}
    write /sys/devices/virtual/android_usb/android0/port_mode 27
    write /sys/class/android_usb/android0/enable 1
    start hdbd
    write /sys/class/net/rndis0/queues/rx-0/rps_cpus f
    setprop sys.usb.state ${sys.usb.config}

on property:sys.usb.config=rndis,adb && property:sys.usb.configfs=0
    write /sys/class/android_usb/android0/enable 0
    write /sys/class/android_usb/android0/idVendor 12d1
    write /sys/class/android_usb/android0/idProduct 108a
    write /sys/class/android_usb/android0/f_rndis/wceis  1
    write /sys/class/android_usb/android0/functions ${sys.usb.config}
    write /sys/devices/virtual/android_usb/android0/port_mode 10
    write /sys/class/android_usb/android0/enable 1
    start adbd
    write /sys/class/net/rndis0/queues/rx-0/rps_cpus f
    setprop sys.usb.state ${sys.usb.config}

on property:sys.usb.config=rndis,adb,hdb && property:sys.usb.configfs=0
    write /sys/class/android_usb/android0/enable 0
    write /sys/class/android_usb/android0/idVendor 12d1
    write /sys/class/android_usb/android0/idProduct 108a
    write /sys/class/android_usb/android0/f_rndis/wceis  1
    write /sys/class/android_usb/android0/functions ${sys.usb.config}
    write /sys/devices/virtual/android_usb/android0/port_mode 28
    write /sys/class/android_usb/android0/enable 1
    start adbd
    start hdbd
    write /sys/class/net/rndis0/queues/rx-0/rps_cpus f
    setprop sys.usb.state ${sys.usb.config}

on property:sys.usb.config=rndis,serial && property:sys.usb.configfs=0
    write /sys/class/android_usb/android0/enable 0
    write /sys/class/android_usb/android0/idVendor 12d1
    write /sys/class/android_usb/android0/idProduct 107e
    write /sys/class/android_usb/android0/f_rndis/wceis  1
    write /sys/class/android_usb/android0/f_hw_acm/hw_instances 3
    write /sys/class/android_usb/android0/functions rndis,hw_acm
    write /sys/devices/virtual/android_usb/android0/port_mode 36
    write /sys/class/android_usb/android0/enable 1
    setprop sys.usb.start_atdiag 1
    write /sys/class/net/rndis0/queues/rx-0/rps_cpus f
    setprop sys.usb.state ${sys.usb.config}

on property:sys.usb.config=rndis,serial,adb && property:sys.usb.configfs=0
    write /sys/class/android_usb/android0/enable 0
    write /sys/class/android_usb/android0/idVendor 12d1
    write /sys/class/android_usb/android0/idProduct 107e
    write /sys/class/android_usb/android0/f_rndis/wceis  1
    write /sys/class/android_usb/android0/f_hw_acm/hw_instances 3
    write /sys/class/android_usb/android0/functions rndis,adb,hw_acm
    write /sys/devices/virtual/android_usb/android0/port_mode 37
    write /sys/class/android_usb/android0/enable 1
    start adbd
    setprop sys.usb.start_atdiag 1
    write /sys/class/net/rndis0/queues/rx-0/rps_cpus f
    setprop sys.usb.state ${sys.usb.config}

on property:sys.usb.config=ptp && property:sys.usb.configfs=0
    write /sys/class/android_usb/android0/enable 0
    write /sys/class/android_usb/android0/idVendor 12d1
    write /sys/class/android_usb/android0/idProduct 107e
    write /sys/class/android_usb/f_mass_storage/luns cdrom
    write /sys/class/android_usb/f_mass_storage/lun/file ${ro.cust.cdrom}
    write /sys/class/android_usb/android0/functions ptp,mass_storage
    write /sys/devices/virtual/android_usb/android0/port_mode 5
    write /sys/class/android_usb/android0/enable 1
    setprop sys.usb.state ${sys.usb.config}

on property:sys.usb.config=ptp,hdb && property:sys.usb.configfs=0
    write /sys/class/android_usb/android0/enable 0
    write /sys/class/android_usb/android0/idVendor 12d1
    write /sys/class/android_usb/android0/idProduct 107e
    write /sys/class/android_usb/f_mass_storage/luns cdrom
    write /sys/class/android_usb/f_mass_storage/lun/file ${ro.cust.cdrom}
    write /sys/class/android_usb/android0/functions ptp,mass_storage,hdb
    write /sys/devices/virtual/android_usb/android0/port_mode 29
    write /sys/class/android_usb/android0/enable 1
    start hdbd
    setprop sys.usb.state ${sys.usb.config}

on property:sys.usb.config=ptp,adb && property:sys.usb.configfs=0
    write /sys/class/android_usb/android0/enable 0
    write /sys/class/android_usb/android0/idVendor 12d1
    write /sys/class/android_usb/android0/idProduct 107e
    write /sys/class/android_usb/f_mass_storage/luns cdrom
    write /sys/class/android_usb/f_mass_storage/lun/file ${ro.cust.cdrom}
    write /sys/class/android_usb/android0/functions ptp,mass_storage,adb
    write /sys/devices/virtual/android_usb/android0/port_mode 6
    write /sys/class/android_usb/android0/enable 1
    start adbd
    setprop sys.usb.state ${sys.usb.config}

on property:sys.usb.config=ptp,adb,hdb && property:sys.usb.configfs=0
    write /sys/class/android_usb/android0/enable 0
    write /sys/class/android_usb/android0/idVendor 12d1
    write /sys/class/android_usb/android0/idProduct 107e
    write /sys/class/android_usb/f_mass_storage/luns cdrom
    write /sys/class/android_usb/f_mass_storage/lun/file ${ro.cust.cdrom}
    write /sys/class/android_usb/android0/functions ptp,mass_storage,adb,hdb
    write /sys/devices/virtual/android_usb/android0/port_mode 30
    write /sys/class/android_usb/android0/enable 1
    start adbd
    start hdbd
    setprop sys.usb.state ${sys.usb.config}

on property:sys.usb.config=mass_storage && property:sys.usb.configfs=0
    write /sys/class/android_usb/android0/enable 0
    write /sys/class/android_usb/android0/idVendor 12d1
    write /sys/class/android_usb/android0/idProduct 107e
    write /sys/class/android_usb/f_mass_storage/luns sdcard
    write /sys/class/android_usb/f_mass_storage/lun/file none
    write /sys/class/android_usb/android0/functions ${sys.usb.config}
    write /sys/devices/virtual/android_usb/android0/port_mode 7
    write /sys/class/android_usb/android0/enable 1
    setprop sys.usb.state ${sys.usb.config}

on property:sys.usb.config=mass_storage,adb && property:sys.usb.configfs=0
    write /sys/class/android_usb/android0/enable 0
    write /sys/class/android_usb/android0/idVendor 12d1
    write /sys/class/android_usb/android0/idProduct 107e
    write /sys/class/android_usb/f_mass_storage/luns sdcard
    write /sys/class/android_usb/f_mass_storage/lun/file none
    write /sys/class/android_usb/android0/functions ${sys.usb.config}
    write /sys/devices/virtual/android_usb/android0/port_mode 8
    write /sys/class/android_usb/android0/enable 1
    start adbd
    setprop sys.usb.state ${sys.usb.config}

on property:sys.usb.config=mass_storage,hdb && property:sys.usb.configfs=0
    write /sys/class/android_usb/android0/enable 0
    write /sys/class/android_usb/android0/idVendor 12d1
    write /sys/class/android_usb/android0/idProduct 107e
    write /sys/class/android_usb/f_mass_storage/luns sdcard
    write /sys/class/android_usb/f_mass_storage/lun/file none
    write /sys/class/android_usb/android0/functions ${sys.usb.config}
    write /sys/devices/virtual/android_usb/android0/port_mode 31
    write /sys/class/android_usb/android0/enable 1
    start hdbd
    setprop sys.usb.state ${sys.usb.config}

on property:sys.usb.config=mass_storage,adb,hdb && property:sys.usb.configfs=0
    write /sys/class/android_usb/android0/enable 0
    write /sys/class/android_usb/android0/idVendor 12d1
    write /sys/class/android_usb/android0/idProduct 107e
    write /sys/class/android_usb/f_mass_storage/luns sdcard
    write /sys/class/android_usb/f_mass_storage/lun/file none
    write /sys/class/android_usb/android0/functions ${sys.usb.config}
    write /sys/devices/virtual/android_usb/android0/port_mode 32
    write /sys/class/android_usb/android0/enable 1
    start adbd
    start hdbd
    setprop sys.usb.state ${sys.usb.config}

on property:sys.usb.config=hisuite,mtp,mass_storage && property:sys.usb.configfs=0
    write /sys/class/android_usb/android0/enable 0
    write /sys/class/android_usb/android0/idVendor 12d1
    write /sys/class/android_usb/android0/idProduct 107e
    write /sys/class/android_usb/f_mass_storage/luns cdrom
    write /sys/class/android_usb/f_mass_storage/lun/file ${ro.cust.cdrom}
    write /sys/class/android_usb/android0/functions mtp,mass_storage
    write /sys/devices/virtual/android_usb/android0/port_mode 35
    write /sys/class/android_usb/android0/enable 1
    setprop sys.usb.state ${sys.usb.config}

on property:sys.usb.config=hisuite,mtp,mass_storage,adb && property:sys.usb.configfs=0
    write /sys/class/android_usb/android0/enable 0
    write /sys/class/android_usb/android0/idVendor 12d1
    write /sys/class/android_usb/android0/idProduct 107e
    write /sys/class/android_usb/f_mass_storage/luns cdrom
    write /sys/class/android_usb/f_mass_storage/lun/file ${ro.cust.cdrom}
    write /sys/class/android_usb/android0/functions mtp,mass_storage,adb
    write /sys/devices/virtual/android_usb/android0/port_mode 15
    write /sys/class/android_usb/android0/enable 1
    start adbd
    setprop sys.usb.state ${sys.usb.config}

on property:sys.usb.config=hisuite,mtp,mass_storage,hdb && property:sys.usb.configfs=0
    write /sys/class/android_usb/android0/enable 0
    write /sys/class/android_usb/android0/idVendor 12d1
    write /sys/class/android_usb/android0/idProduct 107e
    write /sys/class/android_usb/f_mass_storage/luns cdrom
    write /sys/class/android_usb/f_mass_storage/lun/file ${ro.cust.cdrom}
    write /sys/class/android_usb/android0/functions mtp,mass_storage,hdb
    write /sys/devices/virtual/android_usb/android0/port_mode 33
    write /sys/class/android_usb/android0/enable 1
    start hdbd
    setprop sys.usb.state ${sys.usb.config}

on property:sys.usb.config=hisuite,mtp,mass_storage,adb,hdb && property:sys.usb.configfs=0
    write /sys/class/android_usb/android0/enable 0
    write /sys/class/android_usb/android0/idVendor 12d1
    write /sys/class/android_usb/android0/idProduct 107e
    write /sys/class/android_usb/f_mass_storage/luns cdrom
    write /sys/class/android_usb/f_mass_storage/lun/file ${ro.cust.cdrom}
    write /sys/class/android_usb/android0/functions mtp,mass_storage,adb,hdb
    write /sys/devices/virtual/android_usb/android0/port_mode 34
    write /sys/class/android_usb/android0/enable 1
    start adbd
    start hdbd
    setprop sys.usb.state ${sys.usb.config}

on property:sys.usb.config=manufacture,adb && property:sys.usb.configfs=0
    write /sys/class/android_usb/android0/enable 0
    write /sys/class/android_usb/android0/idVendor 12d1
    write /sys/class/android_usb/android0/idProduct 107e
    write /sys/class/android_usb/f_mass_storage/luns sdcard
    write /sys/class/android_usb/f_mass_storage/lun/file none
    write /sys/class/android_usb/android0/f_hw_acm/hw_instances 3
    write /sys/class/android_usb/android0/functions hw_acm,mass_storage,adb,hdb
    write /sys/devices/virtual/android_usb/android0/port_mode 14
    write /sys/class/android_usb/android0/enable 1
    start adbd
    start hdbd
    setprop sys.usb.start_atdiag 1
    setprop sys.usb.start_modempcui 0
    setprop sys.usb.state ${sys.usb.config}

on property:sys.usb.config=manufacture_sn,adb && property:sys.usb.configfs=0
    write /sys/class/android_usb/android0/enable 0
    write /sys/class/android_usb/android0/idVendor 12d1
    write /sys/class/android_usb/android0/idProduct 107e
    write /sys/class/android_usb/f_mass_storage/luns sdcard
    write /sys/class/android_usb/f_mass_storage/lun/file none
    write /sys/class/android_usb/android0/f_hw_acm/hw_instances 3
    write /sys/class/android_usb/android0/functions hw_acm,mass_storage,adb,hdb
    write /sys/devices/virtual/android_usb/android0/port_mode 13
    write /sys/class/android_usb/android0/enable 1
    start adbd
    start hdbd
    setprop sys.usb.start_atdiag 1
    setprop sys.usb.start_modempcui 0
    setprop sys.usb.state ${sys.usb.config}

on property:sys.usb.config=pcmodem1 && property:sys.usb.configfs=0
    write /sys/class/android_usb/android0/enable 0
    write /sys/class/android_usb/android0/idVendor 12d1
    write /sys/class/android_usb/android0/idProduct 1031
    write /sys/class/android_usb/f_mass_storage/luns cdrom
    write /sys/class/android_usb/f_mass_storage/lun/file /data/cust/pcmodem/autorun.iso
    write /sys/class/android_usb/android0/functions mass_storage
    write /sys/devices/virtual/android_usb/android0/port_mode 16
    write /sys/class/android_usb/android0/enable 1
    setprop sys.usb.state $sys.usb.config

on property:sys.usb.config=pcmodem2 && property:sys.usb.configfs=0
    write /sys/class/android_usb/android0/enable 0
    write /sys/class/android_usb/android0/idVendor 12d1
    write /sys/class/android_usb/android0/idProduct 107e
    write /sys/class/android_usb/f_mass_storage/luns cdrom
    write /sys/class/android_usb/f_mass_storage/lun/file /data/cust/pcmodem/autorun.iso
    write /sys/class/android_usb/android0/f_hw_acm/hw_instances 3
    write /sys/class/android_usb/android0/functions hw_acm,mass_storage
    write /sys/devices/virtual/android_usb/android0/port_mode 17
    write /sys/class/android_usb/android0/enable 1
    setprop sys.usb.start_atdiag 0
    setprop sys.usb.start_modempcui 1
    setprop sys.usb.state ${sys.usb.config}

on property:sys.usb.config=pcmodem2,adb && property:sys.usb.configfs=0
    write /sys/class/android_usb/android0/enable 0
    write /sys/class/android_usb/android0/idVendor 12d1
    write /sys/class/android_usb/android0/idProduct 107e
    write /sys/class/android_usb/f_mass_storage/luns cdrom
    write /sys/class/android_usb/f_mass_storage/lun/file /data/cust/pcmodem/autorun.iso
    write /sys/class/android_usb/android0/f_hw_acm/hw_instances 3
    write /sys/class/android_usb/android0/functions hw_acm,mass_storage,adb
    write /sys/devices/virtual/android_usb/android0/port_mode 18
    write /sys/class/android_usb/android0/enable 1
    start adbd
    setprop sys.usb.start_atdiag 0
    setprop sys.usb.start_modempcui 1
    setprop sys.usb.state ${sys.usb.config}

on property:sys.usb.config=midi && property:sys.usb.configfs=0
    write /sys/class/android_usb/android0/enable 0
    write /sys/class/android_usb/android0/idVendor 18d1
    write /sys/class/android_usb/android0/idProduct 4ee9
    write /sys/class/android_usb/android0/functions ${sys.usb.config}
    write /sys/class/android_usb/android0/enable 1
    setprop sys.usb.state ${sys.usb.config}

on property:sys.usb.config=midi,adb && property:sys.usb.configfs=0
    write /sys/class/android_usb/android0/enable 0
    write /sys/class/android_usb/android0/idVendor 18d1
    write /sys/class/android_usb/android0/idProduct 4ee9
    write /sys/class/android_usb/android0/functions ${sys.usb.config}
    write /sys/class/android_usb/android0/enable 1
    start adbd
    setprop sys.usb.state ${sys.usb.config}

 on property:sys.usb.start_atdiag=1
    start diagserver
    start atcmdserver

 on property:sys.usb.start_atdiag=0
    stop diagserver
    stop atcmdserver

 on property:sys.usb.start_modempcui=1
    start hw_modem_service
    start hw_pcui_service

 on property:sys.usb.start_modempcui=0
    stop hw_modem_service
    stop hw_pcui_service